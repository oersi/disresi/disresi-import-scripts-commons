import unittest

from search_index_import_commons.GitHelpers import GitMetadataLoader, GitConnector


class DummyConnector(GitConnector):
    def set_default_user_agent(self, user_agent):
        pass

    def get_file_content_from_repo(self, data, filename):
        if data["id"] == "test1":
            if filename == "metadata.yml":
                return """!include resources/abcd/*.yml"""
            if filename == "resources/abcd/test1.yml":
                return "name: Test1"
        if data["id"] == "test2":
            if filename == "metadata.yml":
                return """!include '*.yml'"""
            if filename == "test2.yml":
                return "name: Test2"
        return None

    def list_files(self, repo_metadata, path):
        if repo_metadata["id"] == "test1":
            return ["test1.yml"]
        if repo_metadata["id"] == "test2":
            return ["test2.yml"]
        return None

    def get_tags(self, repo_metadata):
        return None


class GitHelpersTest(unittest.TestCase):

    def test_metadata_loader_with_subdirectory(self):
        metadata = GitMetadataLoader(DummyConnector()).load_metadata(1, {"id": "test1"})
        self.assertEqual(metadata, [{"name": "Test1"}])

    def test_metadata_loader_without_subdirectory(self):
        metadata = GitMetadataLoader(DummyConnector()).load_metadata(2, {"id": "test2"})
        self.assertEqual(metadata, [{"name": "Test2"}])
