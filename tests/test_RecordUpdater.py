import search_index_import_commons.RecordUpdater as RecordUpdater
from search_index_import_commons.test.BaseImportTest import BaseImportTest


class RecordUpdaterTest(BaseImportTest):

    # @mock.patch('twilloImport.TwilloImport.process_single_update', side_effect=lambda record_id: True)
    # def test_update(self, mock_twillo):
    #     res = RecordUpdater.update_record("https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123")
    #     self.assertTrue(res)

    def test_non_matching_update(self):
        res = RecordUpdater.update_record("https://www.invalid.org/some-non-existing/id")
        self.assertFalse(res)
