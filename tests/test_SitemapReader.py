import unittest
from unittest import mock
from search_index_import_commons.SitemapReader import SitemapReader

TEST_SITEMAP = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"
        xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
        xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0"
        xmlns:pagemap="http://www.google.com/schemas/sitemap-pagemap/1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <url>
        <loc>https://someurl.com/materials/podcasts-synergie-05</loc>
        <lastmod>2019-04-07T18:31:08+02:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>https://someurl.com/footer-items/fe37dcb5-be63-4939-bdf9-17626a95a556</loc>
        <lastmod>2019-03-08T12:00:00+01:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>https://someurl.com/projects/medien-40</loc>
        <lastmod>2020-03-12T18:01:19+01:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
</urlset>"""

TEST_SITEMAPINDEX = """<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc>https://someurl.com/sitemap.xml</loc>
	</sitemap>
</sitemapindex>
"""


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, content, status_code):
            self.content = content
            self.status_code = status_code

    if args[0] == 'https://someurl.com/sitemap.xml':
        return MockResponse(TEST_SITEMAP, 200)
    elif args[0] == 'https://someurl.com/sitemapindex.xml':
        return MockResponse(TEST_SITEMAPINDEX, 200)

    return MockResponse(None, 404)


class SitemapReaderTest(unittest.TestCase):
    user_agent = "SearchIndexImportTestBot"

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_url_locations(self, mock_get):
        sitemap = SitemapReader("https://someurl.com/sitemap.xml", user_agent=self.user_agent)
        urls = sitemap.get_url_locations()
        self.assertEqual(3, len(urls), "Invalid list length")

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_url_locations_with_pattern(self, mock_get):
        sitemap = SitemapReader("https://someurl.com/sitemap.xml", user_agent=self.user_agent,
                                url_pattern=".*/(materials|projects)/.*")
        urls = sitemap.get_url_locations()
        self.assertEqual(2, len(urls), "Invalid list length")

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_url_locations_not_found(self, mock_get):
        sitemap = SitemapReader("https://notfound.com/sitemap.xml", user_agent=self.user_agent)
        with self.assertRaises(IOError):
            sitemap.get_url_locations()

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_url_locations_with_index(self, mock_get):
        sitemap = SitemapReader("https://someurl.com/sitemapindex.xml", user_agent=self.user_agent)
        urls = sitemap.get_url_locations()
        self.assertEqual(3, len(urls), "Invalid list length")

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_url_locations_with_pattern_with_index(self, mock_get):
        sitemap = SitemapReader("https://someurl.com/sitemapindex.xml", user_agent=self.user_agent,
                                url_pattern=".*/(materials|projects)/.*", sitemap_pattern=".*someurl.com/sitemap.xml")
        urls = sitemap.get_url_locations()
        self.assertEqual(2, len(urls), "Invalid list length")


if __name__ == '__main__':
    unittest.main()
