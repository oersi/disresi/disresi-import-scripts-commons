import unittest
import json
from unittest.mock import patch
from search_index_import_commons.test.BaseImportTest import MockResponse
from search_index_import_commons.GitLabImporterBase import GitLabImporterBase


mock_load_next_content = json.dumps([
    {
          "id" : 12345678,
          "description" : "",
          "name" : "Association of Mythographers",
          "name_with_namespace" : "Association of Mythographers / IMT / Courses",
          "path" : "eulist",
          "path_with_namespace" : "association-of-mythographers/imt/courses",
          "created_at" : "2024-12-13T14:35:29.055Z",
          "default_branch" : "main",
          "tag_list" : [ "University Courses" ],
          "topics" : [ "University Courses" ],
          "ssh_url_to_repo" : "git@gitlab.com:association-of-mythographers/imt/courses.git",
          "http_url_to_repo" : "https://gitlab.com/association-of-mythographers/imt/courses.git",
          "web_url" : "https://gitlab.com/association-of-mythographers/imt/courses",
          "readme_url" : None,
          "forks_count" : 0,
          "avatar_url" : None,
          "star_count" : 0,
          "last_activity_at" : "2024-12-17T13:31:55.977Z",
          "namespace" : {
            "id" : 12345678,
            "name" : "Courses",
            "path" : "courses",
            "kind" : "group",
            "full_path" : "association-of-mythographers/imt/courses",
            "parent_id" : 34567890,
            "avatar_url" : None,
            "web_url" : "https://gitlab.com/groups/association-of-mythographers/imt/courses"
          }
}, {
          "id" : 98765432,
          "description" : "",
          "name" : "Chronos Academy of Temporal Studies",
          "name_with_namespace" : "ITS / Courses / Chronos Academy of Temporal Studies",
          "path" : "analysis-1",
          "path_with_namespace" : "ITS/courses/chronos",
          "created_at" : "2024-05-16T12:43:09.190Z",
          "default_branch" : "main",
          "tag_list" : [ "University Courses" ],
          "topics" : [ "University Courses" ],
          "ssh_url_to_repo" : "git@gitlab.com:ITS/courses/chronos.git",
          "http_url_to_repo" : "https://gitlab.com/ITS/courses/chronos.git",
          "web_url" : "https://gitlab.com/ITS/courses/chronos",
          "readme_url" : None,
          "forks_count" : 0,
          "avatar_url" : None,
          "star_count" : 0,
          "last_activity_at" : "2024-07-17T12:16:44.804Z",
          "namespace" : {
            "id" : 98765432,
            "name" : "Chronos Academy of Temporal Studies",
            "path" : "chronos",
            "kind" : "group",
            "full_path" : "ITS/courses/chronos",
            "parent_id" : 85540650,
            "avatar_url" : None,
            "web_url" : "https://gitlab.com/groups/ITS/courses/chronos"
          }
    }
])

mock_get_file_content_from_repo_content_no_spaces_in_filename = json.dumps({
    "file_name": "Mythography.yml",
    "file_path": "courses/Mythography.yml",
    "size": 612,
    "encoding": "base64",
    "content_sha256": "f8b3a9dfd7a2c789654e6f8e73f2b1f7e9d6b3a8c2a9712e89c4f6e73b7a93e1",
    "ref": "main",
    "blob_id": "7a4c98af5d6b89e1a4c3f7d2e6b3f8c7a2b9814f",
    "commit_id": "3e7b2d9c8f5a3e1c4f6b98a7d2e3f6c5b7a89e1f",
    "last_commit_id": "4c7d2e3f8b5a98a1f6e3c4f7d2a98b7e1c6f5b3e",
    "execute_filemode": False,
    "content": "Y291cnNlOgogIGZpZWxkc09mU3R1ZHk6IEh1bWFuaXRpZXMsIE15dGhsb2d5LCBNb2Rlcm4gTGVnZW5kcywgQ3VsdHVyYWwgU3R1ZGllcwogIGxldmVsOiBCLlNjLgogIHN0dWR5TG9hZDoKICAgIHN0dWR5TG9hZFVuaXQ6IGVjdHMKICAgIHZhbHVlOiAzLjAKZXh0OgogIHN0YXR1czogRHJhZnQKbGluazogaHR0cHM6Ly93d3cubXl0aG9ncmFwaHkub3JnL2NvdXJzZXMKbW9kZU9mRGVsaXZlcnk6IEh5YnJpZApuYW1lOgogIC0gbGFuZ3VhZ2U6IGVuCiAgICB2YWx1ZTogTXl0aG9ncmFwaHkKb3JnYW5pemF0aW9uOgogIG5hbWU6CiAgLSBsYW5ndWFnZTogZW4KICAgIHZhbHVlOiBJbnN0aXR1dGUgb2YgTXl0aHMgJiBMZWdlbmRzCiAgLSBsYW5ndWFnZTogZmkKICAgIHZhbHVlOiBNeXl0dGllbiBqYSBsZWdlbmRvamVuIGluc3RpdHV1dHRpCiAgcHJpbWFyeUNvZGU6CiAgICBjb2RlOiBodHRwczovL2V0aC1zdHVkaWVzLm9yZy9vcmdzL0lNTAogICAgY29kZVR5cGU6IGlkZW50aWZpZXI="
})

mock_get_file_content_from_repo_content_spaces_in_filename = json.dumps({
    "file_name": "Time Travel Ethics.yml",
    "file_path": "courses/Time Travel Ethics.yml",
    "size": 589,
    "encoding": "base64",
    "content_sha256": "d9e8f41d7315a54d2d6c475abe3f6c5fdc4c1d44a0e7159876f5ad6b243ad3c8",
    "ref": "main",
    "blob_id": "7f2a85cf9a12a6a84358b4758b2a2c283a6b8721",
    "commit_id": "3a8c12cf8e7b4f93f4316a9e64b670ed9c534b41",
    "last_commit_id": "5e31c78f7a256f93d8f9e5c52c4c3f98317b6723",
    "execute_filemode": False,
    "content": "Y291cnNlOgogIGZpZWxkc09mU3R1ZHk6IFBoaWxvc29waHksIFRpbWUgUGFyYWRveGVzLCBVdG9waWFuIEV0aGljcwogIGxldmVsOiBNLlNjLgogIHN0dWR5TG9hZDoKICAgIHN0dWR5TG9hZFVuaXQ6IGVjdHMKICAgIHZhbHVlOiA2CmV4dDoKICBzdGF0dXM6IERyYWZ0Cmxpbms6ICdodHRwczovL3d3dy5ldGhpY2FsdGltZXRyYXZlbC5lZHUvc3R1ZGllcy9jb3Vyc2VzL3JlbGF0aXZlLXRpbWUtZXRoaWNzJwptb2RlT2ZEZWxpdmVyeTogT25saW5lIFdpdGggSW50ZXJhY3RpdmUgTWlzc2lvbnMKbmFtZToKICAtIGxhbmd1YWdlOiBlbgogICAgdmFsdWU6IFRpbWUgVHJhdmVsIEV0aGljcwpvcmdhbml6YXRpb246CiAgbmFtZToKICAtIGxhbmd1YWdlOiBlbgogICAgdmFsdWU6IENocm9ub3MgQWNhZGVteSBvZiBUZW1wb3JhbCBTdHVkaWVzCiAgLSBsYW5ndWFnZTogZnIKICAgIHZhbHVlOiBBY2Fkw6ltaWUgZGVzIMOpdHVkZXMgdGVtcG9yZWxsZXMgZGUgQ2hyb25vcwogIHByaW1hcnlDb2RlOgogICAgY29kZTogaHR0cHM6Ly91bmltYWdpbmFyeS5vcmcvY2hyb25vcwogICAgY29kZVR5cGU6IGlkZW50aWZpZXI="
})


MOCK_REPO_RESP = [MockResponse(content=mock_get_file_content_from_repo_content_no_spaces_in_filename), MockResponse(content=mock_get_file_content_from_repo_content_spaces_in_filename)]


def mock_get(*args, **kwargs):
    if kwargs['params'].get('topic'):
        return MockResponse(content=mock_load_next_content)
    else:
        return MOCK_REPO_RESP.pop(0)


class DummyGitLabImporter(GitLabImporterBase):

    def __init__(self, name, gitlab_domain, default_gitlab_pages_domain=None):
        super().__init__(name, gitlab_domain, None, 'University Courses', default_gitlab_pages_domain)

    def should_import(self, metadata) -> bool:
        return True


class GitLabImporterBaseTest(unittest.TestCase):

    @patch(target='requests.get', side_effect=mock_get)
    def test_url_encoding(self, mock):
        test_import = DummyGitLabImporter('GitLab', 'gitlab.com')
        records = test_import.load_next()
        # check that all records found
        self.assertEqual(2, len(records), 'Incorrect number of records returned')
        # check that a course name has a space in it
        course_names = [record['resource_metadata']['name'][0]['value'] for record in records]
        self.assertIn(' ', ''.join(course_names))


if __name__ == '__main__':
    unittest.main()
