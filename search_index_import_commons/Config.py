import yaml
import logging
import logging.config


def get_config():
    with open('config/config.yml', 'r') as config_file:
        try:
            return yaml.safe_load(config_file)
        except yaml.YAMLError:
            logging.exception("Could not load config-file")
            return {}


logging_initialized = False

def init_logging(log_file_path="config/logging.conf", reload_config=False):
    global logging_initialized
    if reload_config or not logging_initialized:
        logging.config.fileConfig(log_file_path, disable_existing_loggers=False)
    logging_initialized = True
