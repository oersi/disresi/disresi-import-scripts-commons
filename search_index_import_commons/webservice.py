from flask import Flask, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from os import getcwd
from urllib.parse import urlparse
from search_index_import_commons.Config import init_logging
import search_index_import_commons.RecordUpdater as RecordUpdater

init_logging(log_file_path=getcwd() + '/config/logging-webservice.conf')
app = Flask(__name__, instance_relative_config=True)
limiter = Limiter(app=app, key_func=get_remote_address)


def is_valid_url(url):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except:
        return False


@app.route("/update-record", methods=['POST'])
@limiter.limit("30 per minute")
def update_record_service():
    record_id = request.form['recordId']
    if not record_id or not is_valid_url(record_id):
        return "Invalid url-param 'recordId'", 500
    if RecordUpdater.update_record(record_id):
        return "OK", 200
    else:
        return "FAILED", 500
