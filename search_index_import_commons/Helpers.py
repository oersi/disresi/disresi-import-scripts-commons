import re


honorific_prefixes = [
    "Apl.", "Dr.", "Prof.", "agr.", "B.A./D.B.A.", "biol. anim.", "biol. hom.", "cult.", "disc. pol.", "-Ing.", "iur.", "iur. can.", "iur. et rer. pol.", "iur. utr.", "jur.", "math.", "med.", "med. dent.", "med. sci.", "med. vet.", "M. Sc.", "nat. med.", "nat. techn.", "oec.", "oec. publ.", "oec. troph.", "paed.", "pharm.", "phil.", "phil. in art.", "phil. nat.", "PH", "rer. agr.", "rer. biol. hum.", "rer. biol. vet.", "rer. cult.", "rer. cur.", "rer. forest.", "rer. hort.", "rer. hum.", "rer. med.", "rer. medic.", "rer. merc.", "rer. mont.", "rer. nat.", "rer. oec.", "rer. physiol.", "rer. pol.", "rer. publ.", "rer. sec.", "rer. silv.", "rer. soc.", "rer. sust.", "rer. tech.", "sc. agr.", "sc. ed.", "sc. hum.", "sc. mus.", "sc. oec.", "sc. phil.", "sc. pol.", "sc. soc.", "sc. techn.", "Sportwiss.", "theol.", "troph.",
    "Dipl.agr.biol.", "Dipl.agr.oec.", "Dipl.-Angl.", "Dipl.-Arch.", "Dipl.-Berufspäd.", "Dipl.-Betriebsw.", "Dipl.-Bibl.", "Dipl.-Biochem.", "Dipl.-Biogeogr.", "Dipl.-Bioinf.", "Dipl.-Biol.", "Dipl.-Biophys.", "Dipl.-Biotechnol.", "Dipl.-Braumeister", "Dipl.-Chem.", "Dipl.-Chem.-Ing.", "Dipl.-Chem.oec.", "Dipl.-Comp.-Math.", "Dipl.-Demogr.", "Dipl.-Des.", "Dipl.-Dolm.", "Dipl.-Dram.", "Dipl.-Forstw.", "Dipl.-Geogr.", "Dipl.-Geoinf.", "Dipl.-Geol.", "Dipl.-Geoökol.", "Dipl.-Geophys.", "Dipl.-Germ.", "Dipl.-Geront.", "Dipl.-Ges.oec.", "Dipl.-Ghl.", "Dipl. GDFS", "Dipl.-Gwl.", "Dipl.-Gyml.", "Dipl.-Hdl.", "Dipl.-Heilpäd.", "Dipl.-Hist.", "Dipl.-Holzwirt", "Dipl.-Humanbiologe", "Dipl. human. biol.", "Dipl.-Hydrol.", "Dipl.-Ind. Arch.", "Dipl.-Inf. oder -Inform.", "Dipl.-Inform. Med.", "Dipl.-Inf.Wirt", "Dipl.-Inf.wiss", "Dipl.-Ing.", "Dipl.-Ing. agr.", "Dipl.-Ing.-Inf.", "Dipl.-Ing. oec.", "Dipl.-Ing.-Päd.", "Dipl.-Ing. silv.", "Dipl.-jur.", "Dipl.-Jur.", "Dipl.-Journ.", "Dipl.-Kff.", "Dipl.-Kffr.", "Dipl.-Kfm.", "Dipl.-Kfm.", "Dipl.-Komm.-Wirt", "Dipl.-Krim.", "Dipl.-Krim.", "Dipl.-Kult.", "Dipl.-Kult.Päd.", "Dipl.-Kult.Man.", "Dipl.-Kulturwirt", "Dipl.-Künstler", "Dipl.-Kunstpädagoge", "Dipl.-Landsch.-ökol.", "Dipl.-Lebensmittelchem.", "Dipl.-Lehrer", "Dipl.-Ling.", "Dipl.-LMChem.", "Dipl.-LM-Ing.", "Dipl.-Logist.", "Dipl.-Math.", "Dipl.-Math. oec.", "Dipl.-Wi.-Math.", "Dipl.-Wirtschaftsmath. Univ.", "Dipl.-Math. techn.", "Dipl.-Med.", "Dipl.-Vet.-Med.", "Dipl.-Med. Päd.", "Dipl.-Medienberater", "Dipl.-Mediengestalter", "Dipl.-Medieninf.", "Dipl.-Medienprakt.", "Dipl.-Medienwirt", "Dipl.-Medienwiss.", "Dipl.-Met.", "Dipl.-Mot.", "Dipl.-Mol.Med.", "Dipl.-Mikrobiol.", "Dipl.-Mil.", "Dipl.-Min.", "Dipl.-NanoSc.", "Dipl.-Nat.", "Dipl.-Hist.Sc", "Dipl.-Neurowiss.", "Dipl.oec.troph.", "Dipl.-Oec.", "Dipl.-Ök.", "Dipl.-oen.", "Dipl.-Orient.", "Dipl.-Orientarchäologe", "Dipl.-Oz.", "Dipl.-Päd.", "Dipl.-Pfl.", "Dipl.-PGW", "Dipl.-Pharm.", "Dipl.-Phil.", "Dipl.-Phys.", "Dipl.-Phys.", "Dipl.-Phys.Ing.", "Dipl.-Phys. oec.", "Dipl.-Pol.", "Dipl.-Prähist.", "Dipl.-Psych.", "Dipl.-Reg.-Wiss.", "Dipl.-Reh.-Päd.", "Dipl. rer. com.", "Dipl. rer. oec.", "Dipl. rer. pol.", "Dipl.-Rom.", "Dipl. sc. pol. Univ.", "Dipl.-Sicherheits.-Ing.", "Dipl.-Soz.Ök.", "Dipl.-Soz.", "Dipl.-Soz. tech.", "Dipl.-Soz.päd.", "Dipl.-Sozw.", "Dipl.-Sozialw.", "Dipl.-Soz.Wiss.", "Dipl.-Sporting.", "Dipl.-Sportl.", "Dipl.-SpOec.", "Dipl.-Sportwiss.", "Dipl.-Sprachm.", "Dipl.-Sprechwiss.", "Dipl.-Staatsw.", "Dipl.-Stat.", "Dipl.-Stom.", "Dipl.-Stomat.", "Dipl.-Systemwiss.", "Dipl.-Tech. Math.", "Dipl.-Technoinform.", "Dipl.-Theol.", "Dipl.-Troph.", "Dipl.-Übersetzer", "Dipl.-Umweltwiss.", "Dipl.-UWT", "Dipl.-Verk.wirtsch.", "Dipl.-Verw. Wiss.", "Dipl.-Volksw.", "Dipl.-Wirt.", "Dipl.-Wirtchem.", "Dipl.-Wirt.-Inf.", "Dipl.-Wirt.-Inform.", "Dipl.-Wirtsch.-Inf.", "Dipl.-Wirtsch.Inf.", "Dipl.-Wirtsch.-Inform.", "Dipl.-Wi.-Ing.", "Dipl.-Wirt.-Ing.", "Dipl.-Wirtsch.-Ing.", "Dipl. iur .oec. univ.", "Dipl.-Wigeo.", "Dipl.-Wirtl.", "Dipl.-Wipäd.", "Dipl.-Wiss.org.", "Dipl.-WiWi."
]


def parse_entity_name(entity_name):
    name = re.sub('\\([^)]+\\)', '', entity_name).strip()
    honorific_prefix = None
    honorific_prefixes_regex = ("(?:(?:" + "|".join(honorific_prefixes) + ") ?)+").replace(".", "\\.")
    name_match = re.match("^(" + honorific_prefixes_regex + ")(.+)", entity_name)
    if name_match:
        honorific_prefix = name_match.group(1).strip()
        name = name_match.group(2).strip()
    return {
        "type": "Person",
        "name": name,
        "honorificPrefix": honorific_prefix
    }


def get(record, field_name):
    return record[field_name] if record and field_name in record else None


def get_list(record, field_name):
    if field_name not in record:
        return []
    value = record[field_name]
    return value if isinstance(value, list) else [value]


def build_user_from_vcard(vcard):
    sep = "_###_"
    user_type = None
    user_name = None
    user_id = None
    vcard_without_linebreak = vcard.replace('\r', '').replace("\n", sep)
    person_match = re.match("^BEGIN:VCARD.*" + sep + "FN:(.*?)" + sep + ".*", vcard_without_linebreak)
    if person_match:
        user_type = "Person"
        user_name = person_match.group(1).strip()
        orcid_match = re.match("^BEGIN:VCARD.*" + sep + "X-ORCID:((?!" + sep + ").*?)" + sep + ".*", vcard_without_linebreak)
        if orcid_match:
            user_id = orcid_match.group(1).strip()
            if user_id.find("orcid.org"):
                user_id = user_id.split("/")[-1]
            user_id = "https://orcid.org/" + user_id.strip()
    else:
        orga_match = re.match("^BEGIN:VCARD.*" + sep + "ORG:(.*?)" + sep + ".*", vcard_without_linebreak)
        if orga_match:
            user_type = "Organization"
            user_name = orga_match.group(1).strip()
    if user_type is None or user_name is None or len(user_name) == 0:
        return None
    return {"type": user_type, "name": user_name, "id": user_id}


def fields_to_list(record_list, field):
    try:
        fields = []
        for r in record_list:
            if field in r:
                fields.append(r[field])
        return fields
    except KeyError:
        print('Key', field, 'not found in', record_list)
        return []


def fill_if_field_exists(record, list_of_fields, last_i=-1):
    """
    So many fields are sometimes filled and sometimes empty.
    This function fills metadata if fields are not empty.
    :param record: the current record
    :param list_of_fields: a list of the json fields we want to check
    :param last_i: last index inside said list, used for recursion
    :returns the value of the last field inside list_of_fields if it exists, else None
    """
    i = last_i + 1

    if record and list_of_fields and list_of_fields[i]:
        if i < (len(list_of_fields) - 1):
            if list_of_fields[i] in record:
                return fill_if_field_exists(record[list_of_fields[i]], list_of_fields, last_i=i)
            else:
                return []
        else:
            if isinstance(record, list):
                return fields_to_list(record, list_of_fields[i])
            try:
                return record[list_of_fields[i]]
            except KeyError:
                return []
    return []


def flatten(list1):
    result_list = []
    for sub_item in list1:
        if isinstance(sub_item, list):
            result_list.extend(flatten(sub_item))
        else:
            result_list.append(sub_item)
    return result_list


def filter_none_values(original_object, recursive=False):
    """Remove entries with None values"""
    if isinstance(original_object, dict):
        return {k: filter_none_values(v, recursive=recursive) if recursive else v for k, v in original_object.items() if v is not None}
    if isinstance(original_object, list):
        return list(map(lambda x: filter_none_values(x, recursive=recursive), original_object)) if recursive else original_object
    return original_object
