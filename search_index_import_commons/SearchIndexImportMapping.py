import abc
import logging


class SearchIndexImportMapping(abc.ABC):
    def __init_fields__(self):
        self.mappings = {}
        self.missing_mappings = {}

    def add_mapping(self, group, mapping, default=None):
        if not hasattr(self, "mappings"):
            self.__init_fields__()
        self.mappings[group] = {"mapping": mapping, "default": default}

    def get(self, group, key):
        if not hasattr(self, "mappings"):
            self.__init_fields__()
        group_mappings = self.mappings.get(group)
        if not group_mappings:
            logging.error("No mapping group '%s'", group)
            return None
        if key in group_mappings["mapping"]:
            result = group_mappings["mapping"].get(key)
        else:
            self.missing_mappings[(group, key)] = self.missing_mappings.get((group, key), 0) + 1
            result = group_mappings["default"]
        return result

    def log_missing_keys(self):
        for (group, key) in sorted(self.missing_mappings.keys(), key=lambda x: (x[0], x[1])):
            logging.info("No mapping defined for '{}':'{}' ({})".format(group, key, self.missing_mappings[(group, key)]))
