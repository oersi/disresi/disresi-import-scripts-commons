import abc


class GitMetadataConverter:
    @abc.abstractmethod
    def transform(self, record, provider_name, provider_id):
        """Transform the given git metadata into search index metadata"""
