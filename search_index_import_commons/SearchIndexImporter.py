import base64
import datetime
import requests
import abc
import logging
from typing import Optional
from search_index_import_commons.Config import get_config, init_logging


def validate_record(record):
    required_fields = ["name", "id"]
    for required_field in required_fields:
        if required_field not in record or record[required_field] is None:
            return False
    return True


def cleanup_record(record):
    for key in record.copy():
        if record[key] is None or isinstance(record[key], list) and len(record[key]) == 0:
            del record[key]


def has_4x_response_code(record):
    return "response_status_code" in record and record["response_status_code"] in [401, 403, 404]


class Stats:
    def __init__(self):
        self.processed = 0
        self.failures = 0
        self.successes = 0
        self.skipped = 0
        self.start_time = datetime.datetime.now()

    def elapsed_time(self):
        return datetime.datetime.now() - self.start_time

    def add_failure(self):
        self.processed += 1
        self.failures += 1

    def add_failures(self, nr_of_failures):
        self.processed += nr_of_failures
        self.failures += nr_of_failures

    def add_success(self):
        self.processed += 1
        self.successes += 1

    def add_successes(self, nr_of_successes):
        self.processed += nr_of_successes
        self.successes += nr_of_successes


class SearchIndexImporter(abc.ABC):
    def __init__(self, mapping=None, config=None):
        self.mapping = mapping
        self.stats = None
        init_logging()
        if config is None:
            self.config = get_config()
        else:
            self.config = config
        self.url = self.config['SEARCH_INDEX_BACKEND']['url']
        self.user = self.config['SEARCH_INDEX_BACKEND']['user']
        self.password = self.config['SEARCH_INDEX_BACKEND']['password']
        self.user_agent = self.config['http']['useragent']

    def create_or_update_in_backend(self, search_index_metadata):
        bulk_size = 25
        valid_records = list(filter(lambda r: validate_record(r), search_index_metadata))
        invalid_records = list(filter(lambda r: not validate_record(r), search_index_metadata))
        if len(invalid_records) > 0:
            self.stats.add_failures(len(invalid_records))
            logging.info("Invalid records: %s", invalid_records)
        for record in valid_records:
            cleanup_record(record)
        split_records = [valid_records[x:x+bulk_size] for x in range(0, len(valid_records), bulk_size)]
        for records in split_records:
            resp = requests.post(self.url + "/bulk",
                                 headers={'Content-Type': 'application/json'},
                                 json=records,
                                 auth=(self.user, self.password))
            if resp.status_code == 200:
                json_resp = resp.json()
                self.stats.add_successes(json_resp["success"])
                self.stats.add_failures(json_resp["failed"])
                if json_resp["failed"] > 0:
                    logging.info("%s updates failed: %s", json_resp["failed"], json_resp["messages"])
            else:
                self.stats.add_failures(len(records))
                logging.info("Failed to write records to backend: %s", resp.text)

    def delete_from_backend(self, main_entity_of_page_id):
        encoded_id = base64.urlsafe_b64encode(main_entity_of_page_id.encode("utf-8")).decode("utf-8")
        resp = requests.delete(self.url + "/source/" + encoded_id, auth=(self.user, self.password))
        if resp.status_code == 200:
            self.stats.add_successes(1)
        else:
            self.stats.add_failures(1)
            logging.info("Failed to delete %s from backend: %s", main_entity_of_page_id, resp.text)

    @abc.abstractmethod
    def get_name(self) -> str:
        """Get the name of this search index importer"""

    @abc.abstractmethod
    def contains_record(self, record_id) -> bool:
        """Check if the record identified by the given url is contained in this source"""

    @abc.abstractmethod
    def load_next(self) -> Optional[list]:
        """Retrieve next portion of data; None, if no more data available"""

    @abc.abstractmethod
    def load_single_record(self, record_id) -> Optional:
        """Retrieve source metadata of the record identified by the given id; None, if no metadata can be loaded"""

    def __get_records_for_id__(self, record_id):
        data = self.load_single_record(record_id)
        if data is None:
            return []
        if isinstance(data, list):
            return data
        return [data]

    @abc.abstractmethod
    def transform(self, data: list) -> list:
        """Transform the given data into search-index-metadata"""

    def process_single_update(self, record_id):
        logging.info("Updating %s", record_id)
        self.stats = Stats()
        try:
            records = self.__get_records_for_id__(record_id)
            deletions = list(filter(has_4x_response_code, records))
            update_records = list(filter(lambda x: not has_4x_response_code(x), records))
            if len(deletions) > 0:
                self.delete_from_backend(record_id)
            if len(update_records) > 0:
                search_index_metadata = self.transform(update_records)
                self.create_or_update_in_backend(search_index_metadata)
            if self.stats.successes > 0:
                logging.info("Processed %s successfully, DURATION: %s", record_id, self.stats.elapsed_time())
                return True
        except Exception as err:
            logging.exception(err)
        logging.error("Updating %s FAILED", record_id)
        return False

    def process(self):
        logging.info("Starting %s import", self.get_name())
        self.stats = Stats()
        try:
            data: list = self.load_next()
            while data is not None:
                search_index_metadata = self.transform(data)
                self.create_or_update_in_backend(search_index_metadata)
                data = self.load_next()

            logging.debug("PROCESSED: %s, SKIPPED: %s", self.stats.processed, self.stats.skipped)
            logging.info("Import channel %s, SUCCESS: %s, FAIL: %s, DURATION: %s",
                         self.get_name(), self.stats.successes, self.stats.failures, self.stats.elapsed_time())
        except Exception as err:
            logging.error("Import channel %s FAILED: %s", self.get_name(), err)
            logging.exception(err)
        if self.mapping:
            self.mapping.log_missing_keys()
