import logging
import re
import requests
import xml.etree.ElementTree as ET


invalid_xml_chars_regex = re.compile(u'[\x01-\x08\x0B\x0C\x0E-\x1F\uD800-\uDFFF\uFFFE\uFFFF]')


def escape_xml_invalid_chars(raw_xml, replacement='?'):
    return invalid_xml_chars_regex.sub(replacement, raw_xml)


class OaiPmhReader:

    def __init__(self, url, metadata_prefix, set_spec, user_agent, requests_verify=True, timeout=30):
        self.url = url
        self.metadata_prefix = metadata_prefix
        self.set_spec = set_spec
        self.resumption_token = None
        self.finished_loading = False
        self.user_agent = user_agent
        self.requests_verify = requests_verify
        self.timeout = timeout

    def load_next(self):
        if self.finished_loading:
            return None
        if self.resumption_token:
            params = {"verb": "ListRecords", "resumptionToken": self.resumption_token}
        else:
            params = {"verb": "ListRecords", "metadataPrefix": self.metadata_prefix}
            if self.set_spec:
                params["set"] = self.set_spec
        headers = {"User-Agent": self.user_agent}
        resp = requests.get(self.url, params=params, headers=headers, verify=self.requests_verify, timeout=self.timeout)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from {}: {}".format(self.url, resp))
        content = escape_xml_invalid_chars(resp.text)
        try:
            record_xml = ET.fromstring(content)
        except ET.ParseError as e:
            logging.debug("Could not parse XML: %s", content)
            raise IOError("Could not parse XML from {}: {}".format(self.url, e))
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/"}
        resumption_token_xml = record_xml.find("oai:ListRecords/oai:resumptionToken", namespaces)
        self.resumption_token = resumption_token_xml.text if resumption_token_xml is not None else None
        if not self.resumption_token:
            self.finished_loading = True
        return record_xml.findall("oai:ListRecords/oai:record", namespaces)
