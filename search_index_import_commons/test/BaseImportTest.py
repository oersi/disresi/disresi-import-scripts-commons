import json
import unittest


class MockResponse:
    def __init__(self, status_code=200, content=None):
        self.content = content
        self.text = content
        self.status_code = status_code

    def json(self):
        return json.loads(self.content)

    def content(self):
        return self.content


def default_backend_mock_requests_post(*args, **kwargs):
    return MockResponse(status_code=200, content=json.dumps({"success": 1, "failed": 0, "messages": []}))


def default_backend_mock_requests_delete(*args, **kwargs):
    return MockResponse(status_code=200)


class BaseImportTest(unittest.TestCase):

    def assert_result_contains_expected_metadata(self, result_metadata, expected_path):
        with open(expected_path) as f:
            expected_metadata = json.loads(f.read())
        for k in expected_metadata:
            self.assertTrue(k in result_metadata, "Expected " + k + " to be in the result metadata")
            self.assertEqual(result_metadata[k], expected_metadata[k])
